import { nom_action } from '../actions/index.js'

export default function(state = default_value, action) {
	switch(action.type) {
		case nom_action:
			return action.payload;
		default:
			return state
	}
}