import React, { Component } from 'react';

import {nom_de_fonction} from "../actions/index";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class App extends Component {
	
	componentWillMount() {
		this.props.nom_de_fonction();
	}
	
	render() {
		const {nom_var} = this.props;
		return (
			<div>React Redux ca marche</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		nom_var: state.nom_var_in_store
	}
};

function mapDispatchToProps(dispatch) {
	return bindActionCreators({nom_de_fonction}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(App);